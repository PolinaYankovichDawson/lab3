//Polina Yankovich ID 1834306 9/9/2022
package linearalgebra;
public class Vector3d {
     private double x;
     private double y;
     private double z;

     public Vector3d(double x,double y,double z){
         this.x=x;
         this.y=y;
         this.z=z;
     }
     Vector3d(Vector3d newVectors){
         this.x=newVectors.x;
         this.y=newVectors.y;
         this.z=newVectors.z;
     }

     public double getX(){
         return this.x;
     }   
     public double getY(){
        return this.y;
     }
     public double getZ(){
        return this.z;  
     }
     
     public double magnitude(){
         return Math.sqrt(Math.pow(this.x,2)+Math.pow(this.y,2)+Math.pow(this.z,2));
     }
     public double dotProduct(Vector3d anotherVector){
        return (this.x*anotherVector.x+this.y*anotherVector.y+this.z*anotherVector.z);
     }
     public Vector3d add(Vector3d addVector){
        Vector3d newVector= new Vector3d(this.x+addVector.x,this.y+addVector.y, this.z+addVector.z);
        return newVector;
     }

}