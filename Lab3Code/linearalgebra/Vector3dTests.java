//Polina Yankovich ID 1834306 9/9/2022
package linearalgebra;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class Vector3dTests {
/*1. You should have one test that creates a
 Vector3d and confirms that the get methods return the
correct results */
@Test
public void testVectorExistance(){
   //assign values to vector testV
   Vector3d testV= new Vector3d(1, 1, 2);
   assertNotNull(testV);
//expected values
    double expectedXvalue=1;
    double expectedYvalue=1;
    double expectedZvalue=2;
    //checks if the get methods are equal to the expected values.
    assertEquals(expectedXvalue,testV.getX());
    assertEquals(expectedYvalue,testV.getY());
    assertEquals(expectedZvalue,testV.getZ());
}
/*2.You should have one test that creates 
a Vector3d and confirms that the magnitude() method
returns the correct result.
*/
@Test
public void magnitudeTestVector(){
    Vector3d testV= new Vector3d(1, 1, 2);
    //not empty object
    assertNotNull(testV);
    //expected value
    double expectedMagnitue=Math.sqrt(6);
    //checks if we get the expected value from (1,1,2)
    assertEquals(expectedMagnitue,testV.magnitude());
}
/*3.You should have one test that creates 
two Vector3ds and confirms that the dotProduct()
method returns the correct result.*/
@Test
public void dotProductTest(){
    //assign vectors (1,1,2) (2,3,4)
    Vector3d testV= new Vector3d(1, 1, 2);
    Vector3d testV2= new Vector3d(2, 3, 4);
    //not empty object
    assertNotNull(testV);
    assertNotNull(testV2);
    //expected value (13.0)
    double expectedDotProduct=13;
    //checks if we get the expected value from (1*2+1*3+2*4)=13;
    assertEquals(expectedDotProduct, testV.dotProduct(testV2));
}
/*4. You should have one test that creates 
two Vector3ds and confirms that the add() method
returns the correct result. */
@Test
public void testAdd(){
    //assign vectors (1,1,2) (2,3,4)
    Vector3d testV= new Vector3d(1, 1, 2);
    Vector3d testV2= new Vector3d(2, 3, 4);
    //not empty object
    assertNotNull(testV);
    assertNotNull(testV2);
    //expected value (3,4,6)
    double expectedX=3;
    double expectedY=4;
    double expectedZ=6;
    //check if the returned Vector3d have the values of (3,4,6) as its (x,y,z)vector
    assertEquals(expectedX,testV.add(testV2).getX());
    assertEquals(expectedY,testV.add(testV2).getY());
    assertEquals(expectedZ,testV.add(testV2).getZ());
    }
}
